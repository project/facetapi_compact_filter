<?php
/**
 * Module settings form.
 */
function facetapi_compact_filter_admin_settings() {
  $form['facetapi_compact_filter_number'] = array(
    '#type' => 'select',
    '#title' => t('Number of blocks'),
    '#multiple' => FALSE,
    '#options' => drupal_map_assoc(range(1, 100)),
    '#description' => t('The number of Facetapi Compact Filter blocks.'),
    '#default_value' => variable_get('facetapi_compact_filter_number', 4),
  );
  return system_settings_form($form, FALSE);
}